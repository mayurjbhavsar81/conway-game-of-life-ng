export class ConwayCell {
    private alive: boolean;

    constructor() { this.alive = true; }

    public isAlive() { return this.alive; }
    public markAsDead() { this.alive = false; }
}
