import { ConwayUniverse } from './conway-universe';
import { IDimention } from 'src/app/interfaces/i-dimnetion.interface';

describe('ConwayUniverse', () => {

    it('should create', () => {
        // Arrange
        const conwayUniverse: ConwayUniverse = new ConwayUniverse();

        // Assert
        expect(conwayUniverse).toBeTruthy();
    });

    it('should assign alive cells in init method', () => {
        // Arrange
        const conwayUniverse: ConwayUniverse = new ConwayUniverse();

        // Assert
        let aliveCells = conwayUniverse.getAliveCells();
        expect(aliveCells.length).toEqual(0);
        const points: IDimention[] = [{ x: 0, y: 0 }, { x: 0, y: 1 }, { x: 1, y: 0 }];

        // Act
        conwayUniverse.init(points);

        // Assert again
        aliveCells = conwayUniverse.getAliveCells();
        expect(aliveCells.length).toEqual(3);
    });

    it('should apply death rule for cell at (1,1) if less than 2 neighbor cells are alive near it', () => {
        // Arrange
        const conwayUniverse: ConwayUniverse = new ConwayUniverse();
        const points: IDimention[] = [{ x: 0, y: 1 }, { x: 1, y: 1 }];
        conwayUniverse.init(points);

        // Assert
        let aliveCells = conwayUniverse.getAliveCells();
        expect(aliveCells.length).toEqual(2);
        const cellBeforeTick = aliveCells.find(ac => ac.x === 1 && ac.y === 1);
        expect(cellBeforeTick).not.toBe(undefined);

        // Act
        conwayUniverse.tick();

        // Assert again
        aliveCells = conwayUniverse.getAliveCells();
        expect(aliveCells.length).toEqual(0);
        const cellAfterTick = aliveCells.find(ac => ac.x === 1 && ac.y === 1);
        expect(cellAfterTick).toBe(undefined);
    });

    it('should apply survive rule for cell at (1,1) if cells (1,0) and (1,2) are alive', () => {
        // Arrange
        const conwayUniverse: ConwayUniverse = new ConwayUniverse();
        const points: IDimention[] = [{ x: 1, y: 0 }, { x: 1, y: 1 }, { x: 1, y: 2 }];
        conwayUniverse.init(points);

        // Assert
        let aliveCells = conwayUniverse.getAliveCells();
        expect(aliveCells.length).toEqual(3);
        const cellBeforeTick = aliveCells.find(ac => ac.x === 1 && ac.y === 1);
        expect(cellBeforeTick).not.toBe(undefined);

        // Act
        conwayUniverse.tick();

        // Assert again
        aliveCells = conwayUniverse.getAliveCells();
        expect(aliveCells.length).toEqual(3);
        const cellAfterTick = aliveCells.find(ac => ac.x === 1 && ac.y === 1);
        expect(cellAfterTick).not.toBe(undefined);
    });

    it(`should apply birth rule for new 4 cells at (0,0),(0,2),(2,0),(2,2) if 4 cells (0,1), (1,0), (1,2), (2,1) are alive`, () => {
        // Arrange
        const conwayUniverse: ConwayUniverse = new ConwayUniverse();
        const points: IDimention[] = [{ x: 0, y: 1 }, { x: 1, y: 0 }, { x: 1, y: 1 }, { x: 1, y: 2 }, { x: 2, y: 1 }];
        conwayUniverse.init(points);

        // Assert
        let aliveCells = conwayUniverse.getAliveCells();
        expect(aliveCells.length).toEqual(5);
        const deadCells = aliveCells.filter(ac =>
            (ac.x === 0 && ac.y === 0) || (ac.x === 0 && ac.y === 2) || (ac.x === 2 && ac.y === 0) || (ac.x === 2 && ac.y === 2)
        );
        expect(deadCells.length).toEqual(0);

        // Act
        conwayUniverse.tick();

        // Assert again
        aliveCells = conwayUniverse.getAliveCells();
        expect(aliveCells.length).toEqual(8);
        const deadCellsComingToLife = aliveCells.filter(ac =>
            (ac.x === 0 && ac.y === 0) || (ac.x === 0 && ac.y === 2) || (ac.x === 2 && ac.y === 0) || (ac.x === 2 && ac.y === 2)
        );
        expect(deadCellsComingToLife.length).toEqual(4);
    });

    it('should apply death rule for cell at (1,1) if greater than 3 cells (0,1), (1,0), (1,2), (2,1) are alive', () => {
        // Arrange
        const conwayUniverse: ConwayUniverse = new ConwayUniverse();
        const points: IDimention[] = [{ x: 0, y: 1 }, { x: 1, y: 0 }, { x: 1, y: 1 }, { x: 1, y: 2 }, { x: 2, y: 1 }];
        conwayUniverse.init(points);

        // Assert
        let aliveCells = conwayUniverse.getAliveCells();
        expect(aliveCells.length).toEqual(5);
        const cellBeforeTick = aliveCells.find(ac => ac.x === 1 && ac.y === 1);
        expect(cellBeforeTick).not.toBe(undefined);

        // Act
        conwayUniverse.tick();

        // Assert again
        aliveCells = conwayUniverse.getAliveCells();
        expect(aliveCells.length).toEqual(8);
        const cellAfterTick = aliveCells.find(ac => ac.x === 1 && ac.y === 1);
        expect(cellAfterTick).toBe(undefined);
    });
});
