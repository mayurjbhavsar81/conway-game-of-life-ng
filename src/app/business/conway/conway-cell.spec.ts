import { ConwayCell } from './conway-cell';

describe('ConwayCell', () => {

    it('should create', () => {
        const conwayCell: ConwayCell = new ConwayCell();
        expect(conwayCell).toBeTruthy();
    });

    it('should get isAlive value as true after creation', () => {
        const conwayCell: ConwayCell = new ConwayCell();
        const isAliveValue = conwayCell.isAlive();
        expect(isAliveValue).toBe(true);
    });

    it('should get isAlive value as false after calling markAsDead', () => {
        const conwayCell: ConwayCell = new ConwayCell();
        conwayCell.markAsDead();
        const isAliveValue = conwayCell.isAlive();
        expect(isAliveValue).toBe(false);
    });
});
