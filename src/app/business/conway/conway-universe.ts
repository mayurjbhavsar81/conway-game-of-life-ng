import { ConwayCell } from './conway-cell';
import { IDimention } from '../../interfaces/i-dimnetion.interface';
import { IUniverse } from '../interfaces/i-universe';

export class ConwayUniverse implements IUniverse {
    private cells: { [id: number]: { [id: number]: ConwayCell } } = null;
    private aliveCells: IDimention[] = [];

    public init(points: IDimention[]): void {
        this.cells = {};
        points.forEach(p => {
            if (!this.cells[p.x]) { this.cells[p.x] = {}; }
            this.cells[p.x][p.y] = new ConwayCell();
        });
        this.aliveCells = points;
    }

    public tick(): void {
        // this.logCells();
        this.applyDeathRules();
        this.applyBirthRules();
        // this.logCells();
        this.doAfterTickEffects();
    }

    public getAliveCells(): IDimention[] { return this.aliveCells; }

    private doAfterTickEffects(): void {
        this.aliveCells = [];
        Object.keys(this.cells).forEach(x => {
            Object.keys(this.cells[x]).forEach(y => {
                if (!this.cells[x][y].isAlive()) {
                    delete this.cells[x][y];
                } else {
                    this.aliveCells.push({ x: parseInt(x, 10), y: parseInt(y, 10) });
                }
            });
        });
    }

    private applyBirthRules(): void {
        const probableCellsForBirth: { [x: number]: { [y: number]: boolean } } = this.findProbableCellsForBirth();
        const newcells: { [id: number]: { [id: number]: ConwayCell } } = {};
        // console.log('probableCellsForBirth -> ', probableCellsForBirth);
        Object.keys(probableCellsForBirth).forEach(x => {
            Object.keys(probableCellsForBirth[x]).forEach(y => {
                if (this.isEligibleForBirth(parseInt(x, 10), parseInt(y, 10))) {
                    if (!newcells[x]) { newcells[x] = {}; }
                    newcells[x][y] = new ConwayCell();
                }
            });
        });
        Object.keys(newcells).forEach(x => {
            Object.keys(newcells[x]).forEach(y => {
                if (!this.cells[x]) { this.cells[x] = {}; }
                this.cells[x][y] = new ConwayCell();
                // console.log('Giving birth on ', x, ' ', y);
            });
        });
    }

    private isEligibleForBirth(x: number, y: number): boolean {
        const neighbourCellsCount: number = this.getNeighbourCellsCount(x, y);
        // console.log(x, y, '->', neighbourCellsCount);
        return (neighbourCellsCount === 3);
    }

    private findProbableCellsForBirth() {
        const probableCellsForBirth: { [x: number]: { [y: number]: boolean } } = {};
        Object.keys(this.cells).forEach(x => {
            Object.keys(this.cells[x]).forEach(y => {
                this.updateProbableNeighbourCellsForBirth(parseInt(x, 10), parseInt(y, 10), probableCellsForBirth);
            });
        });
        return probableCellsForBirth;
    }

    private updateProbableNeighbourCellsForBirth(
        x: number, y: number,
        probableCellsForBirth: { [x: number]: { [y: number]: boolean; }; }
    ) {
        if (!this.cells[x - 1] || !this.cells[x - 1][y - 1]) {
            this.addPointToProbableCellsForBirth(x - 1, y - 1, probableCellsForBirth);
        }
        if (!this.cells[x - 1] || !this.cells[x - 1][y]) {
            this.addPointToProbableCellsForBirth(x - 1, y, probableCellsForBirth);
        }
        if (!this.cells[x - 1] || !this.cells[x - 1][y + 1]) {
            this.addPointToProbableCellsForBirth(x - 1, y + 1, probableCellsForBirth);
        }
        if (!this.cells[x] || !this.cells[x][y - 1]) {
            this.addPointToProbableCellsForBirth(x, y - 1, probableCellsForBirth);
        }
        if (!this.cells[x] || !this.cells[x][y + 1]) {
            this.addPointToProbableCellsForBirth(x, y + 1, probableCellsForBirth);
        }
        if (!this.cells[x + 1] || !this.cells[x + 1][y - 1]) {
            this.addPointToProbableCellsForBirth(x + 1, y - 1, probableCellsForBirth);
        }
        if (!this.cells[x + 1] || !this.cells[x + 1][y]) {
            this.addPointToProbableCellsForBirth(x + 1, y, probableCellsForBirth);
        }
        if (!this.cells[x + 1] || !this.cells[x + 1][y + 1]) {
            this.addPointToProbableCellsForBirth(x + 1, y + 1, probableCellsForBirth);
        }
    }

    private addPointToProbableCellsForBirth(x: number, y: number, probableCellsForBirth: { [x: number]: { [y: number]: boolean; }; }) {
        if (!probableCellsForBirth[x]) { probableCellsForBirth[x] = {}; }
        probableCellsForBirth[x][y] = true;
    }

    private applyDeathRules() {
        Object.keys(this.cells).forEach(x => {
            Object.keys(this.cells[x]).forEach(y => {
                this.applyDeathRulesToCell(parseInt(x, 10), parseInt(y, 10));
            });
        });
    }

    private applyDeathRulesToCell(x: number, y: number) {
        const neighbourCellsCount: number = this.getNeighbourCellsCount(x, y);
        if (neighbourCellsCount < 2 || neighbourCellsCount > 3) {
            this.cells[x][y].markAsDead();
        }
    }

    private getNeighbourCellsCount(x: number, y: number): number {
        let count = 0;
        if (this.cells[x - 1] && this.cells[x - 1][y - 1]) { count++; }
        if (this.cells[x - 1] && this.cells[x - 1][y]) { count++; }
        if (this.cells[x - 1] && this.cells[x - 1][y + 1]) { count++; }
        if (this.cells[x] && this.cells[x][y - 1]) { count++; }
        if (this.cells[x] && this.cells[x][y + 1]) { count++; }
        if (this.cells[x + 1] && this.cells[x + 1][y - 1]) { count++; }
        if (this.cells[x + 1] && this.cells[x + 1][y]) { count++; }
        if (this.cells[x + 1] && this.cells[x + 1][y + 1]) { count++; }
        return count;
    }

    // private logCells() {
    //     Object.keys(this.cells).forEach(x => {
    //         Object.keys(this.cells[x]).forEach(y => {
    //             // console.log(this.cells[x][y].isAlive());
    //             if (this.cells[x][y].isAlive()) {
    //                 console.log(`x:${x}, y:${y}`);
    //             }
    //         });
    //     });
    // }
}
