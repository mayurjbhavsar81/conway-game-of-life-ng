import { IDimention } from 'src/app/interfaces/i-dimnetion.interface';

export interface IUniverse {
    init(points: IDimention[]): void;
    tick(): void;
    getAliveCells(): IDimention[];
}
