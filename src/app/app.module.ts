import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlayGroundComponent } from './components/play-ground/play-ground.component';
import { UniverseComponent } from './components/universe/universe.component';
import { LifeComponent } from './components/life/life.component';

@NgModule({
  declarations: [
    AppComponent,
    PlayGroundComponent,
    UniverseComponent,
    LifeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,

    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
