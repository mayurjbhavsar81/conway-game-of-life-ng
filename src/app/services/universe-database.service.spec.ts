import { TestBed, tick, fakeAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { UniverseDatabaseService } from './universe-database.service';

describe('UniverseDatabaseService', () => {
  let service: UniverseDatabaseService;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule]
  }));

  beforeEach(() => {
    service = TestBed.get(UniverseDatabaseService);
    service.timeInterval = 5000; // 5 seconds
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should initialize universe on received points', () => {
    const httpMock = TestBed.get(HttpTestingController);
    spyOn(service.universe, 'init');
    service.init();
    const points = [[80, 35], [80, 36], [81, 35], [81, 36]];
    httpMock.expectOne(service.rndUrl).flush(points);
    expect(service.universe.init).toHaveBeenCalled();
  });

  it('should not initialize universe on error due to http', () => {
    const httpMock = TestBed.get(HttpTestingController);
    spyOn(service.universe, 'init');
    service.init();
    httpMock.expectOne(service.rndUrl).error({ error: 'Raised error' });
    expect(service.universe.init).not.toHaveBeenCalled();
  });

  it('should call startTickTimer method on receiving points', fakeAsync(() => {
    const httpMock = TestBed.get(HttpTestingController);
    spyOn(service.universe, 'init');
    spyOn(service, 'startTickTimer');
    service.init();
    const points = [[80, 35], [80, 36], [81, 35], [81, 36]];
    httpMock.expectOne(service.rndUrl).flush(points);
    expect(service.universe.init).toHaveBeenCalled();
    expect(service.startTickTimer).toHaveBeenCalled();
  }));

  it('should call universe.tick and startTickTimer methods within tick method', fakeAsync(() => {
    spyOn(service.universe, 'tick');
    spyOn(service, 'startTickTimer');
    service.onTick();
    expect(service.universe.tick).toHaveBeenCalled();
    expect(service.startTickTimer).toHaveBeenCalled();
  }));

  it('should call onTick method after certain interval within startTickTimer method', fakeAsync(() => {
    spyOn(service, 'onTick');
    service.startTickTimer();
    tick(service.timeInterval);
    expect(service.onTick).toHaveBeenCalled();
  }));
});
