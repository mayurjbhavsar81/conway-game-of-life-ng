import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, timer, interval } from 'rxjs';

import { IDimention } from '../interfaces/i-dimnetion.interface';
import { IUniverse } from '../business/interfaces/i-universe';
import { ConwayUniverse } from '../business/conway/conway-universe';

@Injectable({
  providedIn: 'root'
})
export class UniverseDatabaseService {
  timeInterval = 500; // Half second

  private nextGeneration: Subject<IDimention[]> = new Subject<IDimention[]>();
  public nextGenerationEvent$: Observable<IDimention[]> = this.nextGeneration.asObservable();

  public universe: IUniverse;

  public url = './assets/points.json';
  public rndUrl = '';

  constructor(private httpClient: HttpClient) {
    this.universe = new ConwayUniverse();
    this.init();
  }

  public init() {
    this.rndUrl = `${this.url}?rnd=${Math.random()}`;
    this.httpClient
      .get<[number, number][]>(this.rndUrl)
      .subscribe((points) => {
        const dimentions: IDimention[] = [];
        points.forEach(p => dimentions.push({ x: p[0], y: p[1] }));
        // console.log(dimentions);

        this.universe.init(dimentions);
        this.startTickTimer();
      }, () => {
        console.log('Failed to get initial points');
      });
  }

  public startTickTimer() {
    this.nextGeneration.next(this.universe.getAliveCells());
    timer(this.timeInterval).subscribe(() => this.onTick());
    // setTimeout(() => this.onTick(), this.timeInterval);
  }

  public onTick() {
    this.universe.tick();
    this.nextGeneration.next(this.universe.getAliveCells());
    this.startTickTimer();
  }

  // Below method is introduced just for achieving 100% code coverage in universe.component
  public setNextGeneration(points: IDimention[]) {
    this.nextGeneration.next(points);
  }
}
