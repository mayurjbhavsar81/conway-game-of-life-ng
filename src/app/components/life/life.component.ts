// 1. A good example of dumb component - it doesn't deal with state of application, just aceepts y and x values and render life
// 2. Reminds me of S - Single Responsibility Principle, cause it is just to render html for life
// 3. Also reminds me of O - Open Closed Principle, cause it is closed for modification,
//    however UI can be extended, necessary properties might be increased
// 4. Also reminds me of DRY - Do not repeat yourself by putting this as html in multiple places if need arises

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-life',
  templateUrl: './life.component.html',
  styleUrls: ['./life.component.scss']
})
export class LifeComponent implements OnInit {
  @Input() y = 0;
  @Input() x = 0;

  private lifeDimentionSize = 10;

  get top() { return this.y * this.lifeDimentionSize + 'px'; }
  get left() { return this.x * this.lifeDimentionSize + 'px'; }
  get height() { return (this.lifeDimentionSize - 1) + 'px'; }
  get width() { return (this.lifeDimentionSize - 1) + 'px'; }

  constructor() { }

  ngOnInit() { }
}
