import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { UniverseComponent } from './universe.component';
import { UniverseDatabaseService } from 'src/app/services/universe-database.service';
import { IDimention } from 'src/app/interfaces/i-dimnetion.interface';

describe('UniverseComponent', () => {
  let component: UniverseComponent;
  let fixture: ComponentFixture<UniverseComponent>;
  let universeDatabaseService: UniverseDatabaseService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [HttpClientTestingModule],
      declarations: [UniverseComponent]
    }).compileComponents();

    universeDatabaseService = TestBed.get(UniverseDatabaseService);
  }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(UniverseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    await fixture.whenStable();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should move universe by 100 X 100 position', () => {
    component.styleMarginTop = '10px';
    component.styleMarginLeft = '10px';
    const xDistance = 100;
    const yDistance = 100;
    component.moveByDistance(xDistance, yDistance);
    expect(component.styleMarginTop).toEqual('110px');
    expect(component.styleMarginLeft).toEqual('110px');
  });

  it('should receive alive cells from next genreation event', () => {
    expect(component.aliveCells).toBe(null);
    const points: IDimention[] = [{ x: 0, y: 0 }, { x: 0, y: 1 }, { x: 1, y: 0 }];
    universeDatabaseService.setNextGeneration(points);
    expect(component.aliveCells.length).toEqual(3);
  });
});
