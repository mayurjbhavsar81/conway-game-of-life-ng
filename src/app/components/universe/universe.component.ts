// 1. A good example of smart component - it deals with state of application
// 2. Reminds me of S - Single Responsibility Principle, cause it just subscribes to next generation event of Universe Datbase Service
// 3. Also remoinds me of O - Open Closed Principle, cause surely it is closed for modification

import { Component, OnInit, HostBinding } from '@angular/core';

import { IDimention } from 'src/app/interfaces/i-dimnetion.interface';
import { UniverseDatabaseService } from 'src/app/services/universe-database.service';

@Component({
  selector: 'app-universe',
  templateUrl: './universe.component.html',
  styleUrls: ['./universe.component.scss']
})
export class UniverseComponent implements OnInit {
  @HostBinding('class') cssClass = 'universe';
  @HostBinding('style.marginLeft') styleMarginLeft = '0px';
  @HostBinding('style.marginTop') styleMarginTop = '0px';

  public aliveCells: IDimention[] = null;

  constructor(private universeDatabaseService: UniverseDatabaseService) { }

  /**
   * Angular Lifecycle - ngOnInit method
   */
  ngOnInit() {
    console.log('Subscribing to universe database events...');
    this.subscribeToUniverseDatabaseEvents();
  }

  /**
   * Moves universe by given x and y distance
   * @param xDistance: X axis distance
   * @param yDistance: Y axis distance
   */
  moveByDistance(xDistance: number, yDistance: number) {
    const currMarginTop = parseInt(this.styleMarginTop.replace('px', ''), 10);
    const currMarginLeft = parseInt(this.styleMarginLeft.replace('px', ''), 10);

    this.styleMarginTop = (currMarginTop + yDistance) + 'px';
    this.styleMarginLeft = (currMarginLeft + xDistance) + 'px';
  }

  /**
   * subscribes to Universe Database service next generation event
   */
  subscribeToUniverseDatabaseEvents() {
    this.universeDatabaseService.nextGenerationEvent$.subscribe(aliveCells => this.aliveCells = aliveCells);
  }
}
