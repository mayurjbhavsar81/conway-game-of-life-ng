// 1. Of couse a smart component - user interaction handling is done here
// 2. Reminds me of S - Single Responsibility Principle, cause it just provides a way to navigate within Universe
//    like a readonly view of Universe
// 3. Also remoinds me of O - Open Closed Principle, cause surely it is closed for modification and open for extension
//    For e.g. drag feature is in place, one may implement arrow key events (top, down, left, right) to navigate

import { Component, OnInit, HostListener, HostBinding, ViewChild } from '@angular/core';

import { UniverseComponent } from '../universe/universe.component';
import { IDimention } from 'src/app/interfaces/i-dimnetion.interface';

@Component({
  selector: 'app-play-ground',
  templateUrl: './play-ground.component.html',
  styleUrls: ['./play-ground.component.scss']
})
export class PlayGroundComponent implements OnInit {

  mousedownEvent: IDimention;
  mouseupEvent: IDimention;
  mousemoveStartEvent: IDimention;
  mousemoveEndEvent: IDimention;

  @HostBinding('class') cssClass = 'play-ground';

  @ViewChild(UniverseComponent, { static: false }) universeCtrl: UniverseComponent;

  constructor() { }

  /**
   * Angular Lifecycle - ngOnInit method
   */
  ngOnInit(): void {
    this._clearDimentionReadings();
  }

  /**
   * Mousedown event handler
   * @param event: MouseEvent
   */
  @HostListener('mousedown', ['$event'])
  onMousedown(event: MouseEvent) {
    this.mousedownEvent = event;
  }

  /**
   * Mousemove event handler
   * @param event: MouseEvent
   */
  @HostListener('mousemove', ['$event'])
  onMousemove(event: MouseEvent) {
    if (!this.mousedownEvent) { return; }

    this.mousemoveEndEvent = event;
    this._scrollInPlaygroundOnMousemove();
  }

  /**
   * Mouseup event handler
   * @param event: MouseEvent
   */
  @HostListener('mouseup', ['$event'])
  @HostListener('document:mouseup', ['$event'])
  onMouseup(event: MouseEvent) {
    if (!this.mousedownEvent) { return; }

    this.mouseupEvent = event;
    this._scrollInPlaygroundOnMouseup();
  }

  /**
   * Scrolls play ground view on drag
   */
  private _scrollInPlaygroundOnMousemove(): void {
    let xDistance: number; let yDistance: number;
    if (this.mousemoveStartEvent && this.mousemoveEndEvent) {
      xDistance = this.mousemoveEndEvent.x - this.mousemoveStartEvent.x;
      yDistance = this.mousemoveEndEvent.y - this.mousemoveStartEvent.y;
      // } else if (this.mousemoveEndEvent && this.mousedownEvent) {
    } else {
      xDistance = this.mousemoveEndEvent.x - this.mousedownEvent.x;
      yDistance = this.mousemoveEndEvent.y - this.mousedownEvent.y;
    }
    this._movePlaygroundView(xDistance, yDistance);
    this.mousemoveStartEvent = this.mousemoveEndEvent;
  }

  /**
   * scrolls play ground view on mouse release
   */
  private _scrollInPlaygroundOnMouseup(): void {
    // console.log('Hey: ', this.mousemoveEndEvent, this.mouseupEvent);

    if (this.mousemoveEndEvent && this.mouseupEvent) {
      const yDistance: number = this.mouseupEvent.y - this.mousemoveEndEvent.y;
      const xDistance: number = this.mouseupEvent.x - this.mousemoveEndEvent.x;
      this._movePlaygroundView(xDistance, yDistance);
    }
    this._clearDimentionReadings();
  }

  /**
   * Moves play ground view by given distance
   * @param xDistance: X axis distance
   * @param yDistance: Y axis distance
   */
  private _movePlaygroundView(xDistance: number, yDistance: number): void {
    // console.log('I have reached here...');
    this.universeCtrl.moveByDistance(xDistance, yDistance);
  }

  /**
   * Clears/Resets dimnetion readings
   */
  private _clearDimentionReadings(): void {
    this.mousedownEvent = null;
    this.mouseupEvent = null;
    this.mousemoveStartEvent = null;
    this.mousemoveEndEvent = null;
  }
}
