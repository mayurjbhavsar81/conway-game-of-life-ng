import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { PlayGroundComponent } from './play-ground.component';
import { UniverseComponent } from '../universe/universe.component';

describe('PlayGroundComponent', () => {
  let component: PlayGroundComponent;
  let fixture: ComponentFixture<PlayGroundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [HttpClientTestingModule],
      declarations: [PlayGroundComponent, UniverseComponent]
    }).compileComponents();
  }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(PlayGroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    await fixture.whenStable();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should trigger moussedown event', () => {
    const xValue = 200;
    const yValue = 200;
    const mousedownEvent: MouseEvent = createMouseEvent(xValue, yValue);
    fixture.debugElement.triggerEventHandler('mousedown', mousedownEvent);
    expect(component.mousedownEvent).not.toBe(null);
    expect(component.mousedownEvent.x).toEqual(xValue);
    expect(component.mousedownEvent.y).toEqual(yValue);
  });

  it('should trigger moussemove event without mousedown event', () => {
    const xValue = 210;
    const yValue = 210;
    const mousemoveEvent: MouseEvent = createMouseEvent(xValue, yValue);
    fixture.debugElement.triggerEventHandler('mousemove', mousemoveEvent);
    expect(component.mousedownEvent).toBe(null);
    expect(component.mousemoveEndEvent).toBe(null);
  });

  it('should trigger moussemove event with mousedown event', () => {
    spyOn(component.universeCtrl, 'moveByDistance');

    let xValue = 200;
    let yValue = 200;
    const mousedownEvent: MouseEvent = createMouseEvent(xValue, yValue);
    fixture.debugElement.triggerEventHandler('mousedown', mousedownEvent);
    xValue = 210;
    yValue = 210;
    const mousemoveEvent: MouseEvent = createMouseEvent(xValue, yValue);
    fixture.debugElement.triggerEventHandler('mousemove', mousemoveEvent);
    expect(component.mousedownEvent).not.toBe(null);
    expect(component.mousemoveEndEvent).not.toBe(null);
    expect(component.universeCtrl.moveByDistance).toHaveBeenCalled();
  });

  it('should trigger mousseup event without mousedown/mousemove event', () => {
    const xValue = 230;
    const yValue = 230;
    const mouseupEvent: MouseEvent = createMouseEvent(xValue, yValue);
    fixture.debugElement.triggerEventHandler('mouseup', mouseupEvent);
    expect(component.mousedownEvent).toBe(null);
    expect(component.mouseupEvent).toBe(null);
  });

  it('should trigger mousseup event with mousedown and without mousemove event', () => {
    spyOn(component.universeCtrl, 'moveByDistance');

    const xValue = 200;
    const yValue = 200;
    const mousedownEvent: MouseEvent = createMouseEvent(xValue, yValue);
    fixture.debugElement.triggerEventHandler('mousedown', mousedownEvent);
    const mouseupEvent: MouseEvent = createMouseEvent(xValue, yValue);
    fixture.debugElement.triggerEventHandler('mouseup', mouseupEvent);
    expect(component.mousedownEvent).toBe(null);
    expect(component.mouseupEvent).toBe(null);
    expect(component.universeCtrl.moveByDistance).not.toHaveBeenCalled();
  });

  it('should trigger mousseup event with mousedown/mousemove event', () => {
    spyOn(component.universeCtrl, 'moveByDistance');

    let xValue = 200;
    let yValue = 200;
    const mousedownEvent: MouseEvent = createMouseEvent(xValue, yValue);
    fixture.debugElement.triggerEventHandler('mousedown', mousedownEvent);
    xValue = 210;
    yValue = 210;
    const mousemoveEvent: MouseEvent = createMouseEvent(xValue, yValue);
    fixture.debugElement.triggerEventHandler('mousemove', mousemoveEvent);
    xValue = 220;
    yValue = 220;
    const mousemove2Event: MouseEvent = createMouseEvent(xValue, yValue);
    fixture.debugElement.triggerEventHandler('mousemove', mousemove2Event);
    xValue = 230;
    yValue = 230;
    expect(component.mousedownEvent).not.toBe(null);
    expect(component.mousemoveEndEvent).not.toBe(null);
    expect(component.universeCtrl.moveByDistance).toHaveBeenCalled();
    const mouseupEvent: MouseEvent = createMouseEvent(xValue, yValue);
    fixture.debugElement.triggerEventHandler('mouseup', mouseupEvent);
    expect(component.mousedownEvent).toBe(null);
    expect(component.mouseupEvent).toBe(null);
    expect(component.universeCtrl.moveByDistance).toHaveBeenCalled();
  });
});

function createMouseEvent(xValue: number, yValue: number) {
  const mousedownEvent: MouseEvent = document.createEvent('MouseEvent');
  mousedownEvent.initMouseEvent('mousedown', true, true, null, 0, xValue, yValue, xValue, yValue, false, false, false, false, 0, null);
  return mousedownEvent;
}
